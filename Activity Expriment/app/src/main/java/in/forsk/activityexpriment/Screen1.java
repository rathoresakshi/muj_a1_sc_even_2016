package in.forsk.activityexpriment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class Screen1 extends AppCompatActivity implements View.OnClickListener {

    private final static String TAG = Screen1.class.getSimpleName();
    Button exitBtn, openScreen2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        NotificationUtils.notify(this, "OnCreate");
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        exitBtn = (Button) findViewById(R.id.button);
        openScreen2 = (Button) findViewById(R.id.button2);

        exitBtn.setOnClickListener(this);
        openScreen2.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        NotificationUtils.notify(this, " OnStart");
    }

    @Override
    protected void onResume() {
        super.onResume();

        NotificationUtils.notify(this, " OnResume");
    }

//    @Override
//    protected void onPause() {
//        super.onPause();
//
//        NotificationUtils.notify(this, " OnPause");
//    }

    @Override
    protected void onStop() {
        super.onStop();

        NotificationUtils.notify(this, " OnStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        NotificationUtils.notify(this, " OnDestroy");
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button:
                //Step 1
                // Createing the Diloag Object.
                //Explain the activity context (this)
                AlertDialog.Builder dialog = new AlertDialog.Builder(Screen1.this);

                //Step 2
                //Setting the properties
                dialog.setTitle("Alert Dialog Box");
                dialog.setMessage("Do you want to close the app !!");
                dialog.setCancelable(false);

                //Step 3
                //Explain Alert Dialog box have 2 Button and their Event handler.

                //Setting the behavior (action) for Yes button
                //OnClickListner Anynomous Class.
                dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
                        finish();
                    }
                });

                //Setting the behavior (action) for No button
                //OnClickListner Anynomous Class.
                dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                //Step 4
                //Show the dialog
                dialog.show();

                break;

            case R.id.button2:
                Intent intent = new Intent(Screen1.this, Screen2.class);
                startActivity(intent);

                break;
        }
    }


}
